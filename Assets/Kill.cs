﻿using UnityEngine;
using System.Collections;

public class Kill : MonoBehaviour
{
    public float lifeTime;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        lifeTime--;
        if (lifeTime<1)
        {
            Destroy(this.gameObject);
        }
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            //Destroy(GameObject);
            Debug.Log("player hit");
        }
    }
}
