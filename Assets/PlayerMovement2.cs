﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement2 : MonoBehaviour
{
    public Vector2 direction;
    public float force;
    public Vector2 directionR;
    public float forceR;
    public int maxJumps;
    public float JumpForce;
    public int coins = 0;
    public Text textField;
    public Text gameOver;
    public Text Death;
    public float Deathcount;
    public GameObject currentCheckpoint;
    public float movespeed;
    public float jumpHeight;
    public float WallJumpHeight;
    public LayerMask groundCheckLayerMask;
    public float wjdelay;
    public GameObject DeathPEW;
    public float RespawnDelay;
    public float RespawnTime;
    private SpriteRenderer Seen;

    private PolygonCollider2D Colli;
    public float movespeedifwj;


    //float jumpTime, JumpDelay = 0.5f;
    bool jumped;

    private Rigidbody2D rb2D;
    private int jumps = 0;
    private bool isJumping = false;
    private float justwalljumped;
    private float alive = 1;



    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        isJumping = false;
        gameOver.text = "";
        rb2D.freezeRotation = true;
        Seen = GetComponent<SpriteRenderer>();
        Colli = GetComponent<PolygonCollider2D>();
    }


    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow) && justwalljumped < 1 && alive == 1)
        {
            rb2D.velocity = new Vector2(movespeed, rb2D.velocity.y);
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if (Input.GetKey(KeyCode.LeftArrow) && justwalljumped < 1 && alive == 1)
        {
            rb2D.velocity = new Vector2(-movespeed, rb2D.velocity.y);
            transform.eulerAngles = new Vector3(0, 180, 0);
        }


        /*if (Input.GetKey(KeyCode.D) && justwalljumped > 1 && alive == 1)
        {
            rb2D.velocity = new Vector2(movespeedifwj, rb2D.velocity.y);
            anim.SetFloat("Speed", 1);
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if (Input.GetKey(KeyCode.A) && justwalljumped > 1 && alive == 1)
        {
            rb2D.velocity = new Vector2(-movespeedifwj, rb2D.velocity.y);
            anim.SetFloat("Speed", 1);
            transform.eulerAngles = new Vector3(0, 180, 0);
        }*/
        if (Input.GetKeyDown(KeyCode.UpArrow) && jumps < maxJumps && RightCheck() == false && LeftCheck() == false && alive == 1)
        {
            rb2D.velocity = new Vector2(rb2D.velocity.x, jumpHeight);
            isJumping = true;
            jumps++;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && RightCheck() == true && LeftCheck() == false && alive == 1)
        {
            rb2D.velocity = new Vector2(-WallJumpHeight, jumpHeight);
            isJumping = true;
            jumps++;
            jumps = 1;
            justwalljumped = wjdelay;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && RightCheck() == false && LeftCheck() == true && alive == 1)
        {
            rb2D.velocity = new Vector2(WallJumpHeight, jumpHeight);
            isJumping = true;
            jumps++;
            jumps = 1;
            justwalljumped = wjdelay;
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow) && justwalljumped < 1)
        {
            rb2D.velocity = new Vector2(0, rb2D.velocity.y);
        }
        if (Input.GetKeyUp(KeyCode.RightArrow) && justwalljumped < 1)
        {
            rb2D.velocity = new Vector2(0, rb2D.velocity.y);
        }


    }

    void FixedUpdate()
    {
        if (justwalljumped > 0)
        {
            justwalljumped--;
        }

        if (RespawnDelay > 0)
        {
            RespawnDelay--;
        }

        if (RespawnDelay == 2)
        {
            transform.position = currentCheckpoint.transform.position;
            Seen.enabled = true;
            Colli.enabled = true;
            rb2D.isKinematic = false;
            alive = 1;
            rb2D.velocity = Vector2.zero;
            // gameObject.SetActive(true);
        }
    }


    void OnCollisionEnter2D(Collision2D other)
    {

        isJumping = false;
        if (CollisionCheck())
        {
            jumps = 0;
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Respawn"))
        {
            currentCheckpoint = other.gameObject;
            Debug.Log("You got a Checkpoint");
        }
        if (other.CompareTag("Finish"))
        {
            Application.LoadLevel("WinScene");
        }


        if (CollisionCheck())
        {
            jumps = 0;
        }

    }

    private bool CollisionCheck()
    {
        RaycastHit2D rayHit = Physics2D.Raycast(this.transform.position + new Vector3(-0.4f, 0, 0),
            Vector2.down, 1, groundCheckLayerMask);
        RaycastHit2D rayHit2 = Physics2D.Raycast(this.transform.position + new Vector3(0.4f, 0, 0),
              Vector2.down, 1, groundCheckLayerMask);

        if (rayHit || rayHit2)
        {
            return true;
        }
        return false;
    }

    private bool RightCheck()
    {
        RaycastHit2D rayHit = Physics2D.Raycast(this.transform.position,
                Vector2.right, 1, groundCheckLayerMask);

        if (rayHit)
        {
            return true;
        }
        return false;
    }

    private bool LeftCheck()
    {
        RaycastHit2D rayHit = Physics2D.Raycast(this.transform.position,
                Vector2.left, 1, groundCheckLayerMask);

        if (rayHit)
        {
            return true;
        }
        return false;
    }

}