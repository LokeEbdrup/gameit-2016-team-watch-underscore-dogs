﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public Vector2 direction;
    public float force;
    public Vector2 directionR;
    public float forceR;
    public int maxJumps;
    public float JumpForce;
    public int coins = 0;
    public Text textField;
    public Text gameOver;
    public Text Death;
    public float Deathcount;
    public GameObject currentCheckpoint;
    public float movespeed;
    public float jumpHeight;
    public float WallJumpHeight;
    public LayerMask groundCheckLayerMask;
    public float wjdelay;
    public GameObject DeathPEW;
    public float RespawnDelay;
    public float RespawnTime;
    private SpriteRenderer Seen;
    public GameObject hitZone;
    public GameObject hitBox;
    public GameObject stunBox;
    public GameObject breakBox;
    public KeyCode upKey;
    public KeyCode downKey;
    public KeyCode rightKey;
    public KeyCode leftKey;
    public KeyCode atkKey;
    public KeyCode blockKey;
    public KeyCode breakKey;
    public bool isBlocking = false;
    public float atkTime;
    public float atkDelay;
    public float atkdelaymax;
    public float atkdelaymin;
    public bool Stunned;
    public float stunDur;
    public float stunTime;
    public float breakDelay;
    public AudioClip Hit;
        public AudioClip Block;
        public AudioClip Break;

    private PolygonCollider2D Colli;
    public float movespeedifwj;


    //float jumpTime, JumpDelay = 0.5f;
    bool jumped;

    private Rigidbody2D rb2D;
    private int jumps = 0;
    private bool isJumping = false;
    private float justwalljumped;
    private float alive = 1;
    private AudioSource source;

    Animator anim;



    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        isJumping = false;
        rb2D.freezeRotation = true;
        Seen = GetComponent<SpriteRenderer>();
        Colli = GetComponent<PolygonCollider2D>();
        anim = GetComponent<Animator>();
        Stunned = false;
        source = GetComponent<AudioSource>();
    }


    void Update()
    {
        if (Input.GetKey(rightKey) && justwalljumped < 1 && alive == 1 && Stunned == false)
        {
            rb2D.velocity = new Vector2(movespeed, rb2D.velocity.y );
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if (Input.GetKey(leftKey) && justwalljumped < 1 && alive == 1 && Stunned == false)
        {
            rb2D.velocity = new Vector2(-movespeed, rb2D.velocity.y);
            transform.eulerAngles = new Vector3(0, 180, 0);
        }


        /*if (Input.GetKey(KeyCode.D) && justwalljumped > 1 && alive == 1)
        {
            rb2D.velocity = new Vector2(movespeedifwj, rb2D.velocity.y);
            anim.SetFloat("Speed", 1);
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if (Input.GetKey(KeyCode.A) && justwalljumped > 1 && alive == 1)
        {
            rb2D.velocity = new Vector2(-movespeedifwj, rb2D.velocity.y);
            anim.SetFloat("Speed", 1);
            transform.eulerAngles = new Vector3(0, 180, 0);
        }*/
        if (Input.GetKeyDown(upKey) && jumps < maxJumps && RightCheck() == false && LeftCheck() == false && alive == 1 && Stunned == false)
        {
            rb2D.velocity = new Vector2(rb2D.velocity.x, jumpHeight);
            isJumping = true;
            jumps++;
        }
        if (Input.GetKeyDown(upKey) && RightCheck() == true && LeftCheck() == false && alive == 1 && Stunned == false)
        {
            rb2D.velocity = new Vector2(-WallJumpHeight, jumpHeight);
            isJumping = true;
            jumps++;
            jumps = 1;
            justwalljumped = wjdelay;
        }
        if (Input.GetKeyDown(upKey) && RightCheck() == false && LeftCheck() == true && alive == 1 && Stunned == false)
        {
            rb2D.velocity = new Vector2(WallJumpHeight, jumpHeight);
            isJumping = true;
            jumps++;
            jumps = 1;
            justwalljumped = wjdelay;
        }
        if (Input.GetKeyUp(rightKey) && justwalljumped < 1 && Stunned == false)
        {
            rb2D.velocity = new Vector2(0, rb2D.velocity.y );
        }
        if (Input.GetKeyUp(leftKey) && justwalljumped < 1 && Stunned == false)
        {
            rb2D.velocity = new Vector2(0, rb2D.velocity.y);
        }
        if (Input.GetKeyDown(atkKey) && atkDelay < 1 && Stunned == false && isBlocking == false && breakDelay < 1)
        {
            anim.SetTrigger("atk");
            //Instantiate(hitBox, hitZone.transform.position, hitZone.transform.rotation);
            atkDelay = atkTime;
        }
        if ( Input.GetKeyDown(breakKey) && breakDelay < 1 && atkDelay < 1 && Stunned == false && isBlocking == false)
        {
            anim.SetTrigger("break");
            breakDelay = atkTime;
        }
        if (Input.GetKeyDown(blockKey) && Stunned == false)
        {
            isBlocking = true;

        }
        if (Input.GetKeyUp(blockKey))
        {
            isBlocking = false;

        }
        if ( isBlocking == true)
        {
            anim.SetBool("isBlock", true);
        }
        if (isBlocking == false)
        {
            anim.SetBool("isBlock", false);
        }
        if (atkDelay < 1)
        {
            anim.SetBool("isAtk", false);
        }
        if (atkDelay > atkdelaymin && atkDelay < atkdelaymax)
        {
            Instantiate(hitBox, hitZone.transform.position, hitZone.transform.rotation);
        }
        if (breakDelay > atkdelaymin && breakDelay < atkdelaymax)
        {
            Instantiate(breakBox, hitZone.transform.position, hitZone.transform.rotation);
        }
        if (Stunned == true)
        {
            anim.SetBool("isStunned", true);
        }
        if (Stunned == false)
        {
            anim.SetBool("isStunned", false);
        }
        if (stunDur > 10)
        {
            Stunned = true;
        }
        if (stunDur < 9)
        {
            Stunned = false;
        }


    }

    void FixedUpdate()
    {
        if (justwalljumped > 0)
        {
            justwalljumped--;
        }

        if (atkDelay > 0)
        {
            atkDelay--;
        }
        if (breakDelay > 0 )
        {
            breakDelay--;
        }

        if (stunDur > 0)
        {
            stunDur--;
        }

        if (RespawnDelay > 0)
        {
            RespawnDelay--;
        }

        if (RespawnDelay == 2)
        {
            transform.position = currentCheckpoint.transform.position;
            Seen.enabled = true;
            Colli.enabled = true;
            rb2D.isKinematic = false;
            alive = 1;
            rb2D.velocity = Vector2.zero;
            // gameObject.SetActive(true);
        }
    }


    void OnCollisionEnter2D(Collision2D other)
    {

        isJumping = false;
        if (CollisionCheck())
        {
            jumps = 0;
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Respawn"))
        {
            currentCheckpoint = other.gameObject;
            Debug.Log("You got a Checkpoint");
        }
        if (other.CompareTag("Sword") && isBlocking == false)
        {
            
            source.PlayOneShot(Hit);
Destroy(this.gameObject);
        }
        if (other.CompareTag("Break") && isBlocking == true)
        {
            stunDur = stunTime;
            source.PlayOneShot(Break);
        }
        if (other.CompareTag("Sword") && isBlocking == true)
        {
            Instantiate(stunBox, hitZone.transform.position, hitZone.transform.rotation);
        }
        if (other.CompareTag("Stun"))
        {
            stunDur = stunTime;
            source.PlayOneShot(Block);
        }


        if (CollisionCheck())
        {
            jumps = 0;
        }

    }

    private bool CollisionCheck()
    {
        RaycastHit2D rayHit = Physics2D.Raycast(this.transform.position + new Vector3(-0.4f, 0, 0),
            Vector2.down, 1, groundCheckLayerMask);
        RaycastHit2D rayHit2 = Physics2D.Raycast(this.transform.position + new Vector3(0.4f, 0, 0),
              Vector2.down, 1, groundCheckLayerMask);

        if (rayHit || rayHit2)
        {
            return true;
        }
        return false;
    }

    private bool RightCheck()
    {
        RaycastHit2D rayHit = Physics2D.Raycast(this.transform.position,
                Vector2.right, 1, groundCheckLayerMask);

        if (rayHit)
        {
            return true;
        }
        return false;
    }

    private bool LeftCheck()
    {
        RaycastHit2D rayHit = Physics2D.Raycast(this.transform.position,
                Vector2.left, 1, groundCheckLayerMask);

        if (rayHit)
        {
            return true;
        }
        return false;
    }

}